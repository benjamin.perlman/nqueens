from pprint import pprint


def is_in_danger(board, row_index, column_index):
    #check the row for a rook
    for index,value in enumerate(board [row_index]):
        if value == 0 or index == column_index:
            pass
        else:
            return True

    #check the col for a rook
    # for index,row in enumerate (board):
    #     if  row[column_index] == 0 or row_index:
    #         pass
    #     else:
    #         return True
    #after checking row and col return false ... chuckles not in danger
    return False

def place_rook_in_column(board, column_index, num):
    #base case
    if column_index >= num:
        return
    for row_number in range(num):
       # print("ro", row_number)
        board[row_number][column_index]=1
       # pprint(board, width=20)
        
        
        #print(is_in_danger(board, row_number, column_index))
        if is_in_danger(board, row_number, column_index):
            #if in danger remove rook
            board[row_number][column_index] = 0
        else:
            place_rook_in_column(board, column_index + 1, num)





def four_rooks(num):
   #num = 4
    #row = []
    board = []
    for i in range(num):
        row = []
        for j in range(num):   
            row.append(0)
                      
        board.append(row) 

        
   # print(board)
    place_rook_in_column(board, 0, num)
    return board


#if __name__ == "__main2__":
board = four_rooks(3)
pprint(board, width=20)
